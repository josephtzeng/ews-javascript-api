"use strict";
const WinJS = require('winjs-node');
const PromiseFactory_1 = require("./PromiseFactory");
function PromiseExport(init, onCancel) {
    return new WinJS.Promise(init, onCancel);
}
class WinJSPromiseApi {
    create(init, onCancel) {
        return new WinJS.Promise(init, onCancel);
    }
    resolve(value) {
        return WinJS.Promise.as(value);
    }
    reject(value) {
        return WinJS.Promise.wrapError(value);
    }
    get type() {
        return "WinJS";
    }
}
PromiseFactory_1.PromiseFactory.switchPromise(new WinJSPromiseApi());
function setPromise() { }
exports.setPromise = setPromise;
