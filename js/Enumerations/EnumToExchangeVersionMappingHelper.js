"use strict";
(function (EnumToExchangeVersionMappingHelper) {
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["WellKnownFolderName"] = 0] = "WellKnownFolderName";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["ItemTraversal"] = 1] = "ItemTraversal";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["ConversationQueryTraversal"] = 2] = "ConversationQueryTraversal";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["FileAsMapping"] = 3] = "FileAsMapping";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["EventType"] = 4] = "EventType";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["MeetingRequestsDeliveryScope"] = 5] = "MeetingRequestsDeliveryScope";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["ViewFilter"] = 6] = "ViewFilter";
    EnumToExchangeVersionMappingHelper[EnumToExchangeVersionMappingHelper["MailboxType"] = 7] = "MailboxType";
})(exports.EnumToExchangeVersionMappingHelper || (exports.EnumToExchangeVersionMappingHelper = {}));
var EnumToExchangeVersionMappingHelper = exports.EnumToExchangeVersionMappingHelper;
