"use strict";
class promiseApi {
    create(init, onCancel) {
        throw new Error("PromiseApi - stub method, must be bootstrapped");
    }
    resolve(value) {
        throw new Error("PromiseApi - stub method, must be bootstrapped");
    }
    reject(value) {
        throw new Error("PromiseApi - stub method, must be bootstrapped");
    }
    get type() {
        return "none";
    }
}
var promiseApiObj = new promiseApi();
class PromiseFactory {
    static create(init, onCancel) {
        return promiseApiObj.create(init, onCancel);
    }
    static resolve(value) {
        return promiseApiObj.resolve(value);
    }
    static reject(value) {
        return promiseApiObj.reject(value);
    }
    static get type() {
        return promiseApiObj.type;
    }
    static get Promise() { return promiseApiObj; }
    static set Promise(value) { promiseApiObj = value; }
    static switchPromise(newPromiseObject) {
        promiseApiObj = newPromiseObject;
    }
}
exports.PromiseFactory = PromiseFactory;
