"use strict";
const WinJS = require('winjs-node');
const XHRFactory_1 = require("./XHRFactory");
class WinJSXHRApi {
    xhr(xhroptions) {
        return WinJS.xhr(xhroptions);
    }
    get type() {
        return "WinJS";
    }
}
exports.WinJSXHRApi = WinJSXHRApi;
XHRFactory_1.XHRFactory.switchXhr(new WinJSXHRApi());
function setXhr() { }
exports.setXhr = setXhr;
