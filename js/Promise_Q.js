"use strict";
const Q = require('q');
const PromiseFactory_1 = require("./PromiseFactory");
function PromiseExport(init, onCancel) {
    return Q.Promise(init);
}
class QPromiseApi {
    create(init, onCancel) {
        return Q.Promise(init);
    }
    resolve(value) {
        return Q(value);
    }
    reject(value) {
        return Q.reject(value);
    }
    get type() {
        return "Q";
    }
}
exports.QPromise = new QPromiseApi();
PromiseFactory_1.PromiseFactory.switchPromise(new QPromiseApi());
function setPromise() { }
exports.setPromise = setPromise;
